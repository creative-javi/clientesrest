package com.labs.creative.clientesrest.ui.listado;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.labs.creative.clientesrest.R;
import com.labs.creative.clientesrest.remote.model.ClienteResponse;

import java.util.List;
import java.util.zip.Inflater;

public class AdapterClientes extends RecyclerView.Adapter<AdapterClientes.ViewHolder>
{

    private List<ClienteResponse> dataSet;
    private Context context;
    private OnItemClickListenerClientes listener;

    //Inicializa las variables del dataset y el contexto en que se ejecuta la apliacacion,
    public AdapterClientes(List<ClienteResponse> dataSet, Context context) {
        this.dataSet = dataSet;
        this.context = context;
    }

    //Enlaza la vista del template o layout con el que se va a trabajar en este caso la plantilla de los items
    //Retorna un viewholder para enlazar las propiedades del cliente a la vista
    @NonNull
    @Override
    public AdapterClientes.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_template_cliente,parent,false);
        return new ViewHolder(view);
    }

    //Liga los valores de los clientes a las vistas con el metodo bind de la clase interna ViewHolder
    @Override
    public void onBindViewHolder(@NonNull AdapterClientes.ViewHolder holder, int position) {
        holder.bind(dataSet.get(position),listener);
    }

    //Devuelve el tamaño de la lista de elementos
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    //Establecemos el listener para los eventos de la lista
    public void setListener(OnItemClickListenerClientes listener)
    {
        this.listener = listener;
    }

    //Helper para enlazar los datos de los clientes a las vistas
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvNombre;
        private TextView tvEmail;
        private ImageButton btnEditar;
        private ImageButton btnEliminar;

        //En el constructor se inicializan y se enlaan los objetos a las vistas de la UI
        public ViewHolder(View itemView)
        {
            super(itemView);
            tvNombre = itemView.findViewById(R.id.tvNombreCliente);
            tvEmail = itemView.findViewById(R.id.tvEmailCliente);
            btnEditar = itemView.findViewById(R.id.ibtnEditar);
            btnEliminar = itemView.findViewById(R.id.ibtnEliminar);
        }

        //Este metodo enlaza las pripiedades a las vistas y agrega los listeners para manejar los eventos
        public void bind(final ClienteResponse item, final OnItemClickListenerClientes listener)
        {
            tvEmail.setText(item.getEmail()); //Asigna el email
            tvNombre.setText(item.getNombre()); //Asigna el Nombre

            //Se agregagan los listeners a los botones.
            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDelete(item);
                }
            });

            btnEditar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onEdit(item);
                }
            });
        }
    }
}
