package com.labs.creative.clientesrest.remote.api;


import com.labs.creative.clientesrest.remote.model.BaseResponse;
import com.labs.creative.clientesrest.remote.model.ClienteResponse;
import com.labs.creative.clientesrest.ui.model.Cliente;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Interfaz que utiliza retrofit para manejar las peticiones al servidor.
 * */
public interface IGetDataService
{
    //Se crean los Handlers para hacer las peticiones al servidor
    //Cada metodo hace la transformacion de JSON a los POJOs que se definen en el modelo.
    @GET("/clientes")
    Call<List<ClienteResponse>> getAllClientes(); //Obtiene todos los clientes

    @POST("/create")
    Call<BaseResponse> createCliente(@Body Cliente cliente);//Crea un nuevo cliente y recibe de parametro el cliente

    @PUT("/update/id")
    Call<BaseResponse> updateCliente(@Body Cliente cliente); //Actualiza un cliente existente y recibe como parametro el cliente a actualizar

    @DELETE("/delete/{id}")
    Call<BaseResponse> deleteCliente(@Path("id") int idCliente); //Borra un cliente y recibe de parametro el id del cliente a borrar

}
