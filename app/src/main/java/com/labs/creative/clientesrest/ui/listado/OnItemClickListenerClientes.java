package com.labs.creative.clientesrest.ui.listado;

import com.labs.creative.clientesrest.remote.model.ClienteResponse;

public interface OnItemClickListenerClientes
{
    void onDelete(ClienteResponse item);
    void onEdit(ClienteResponse item);
}
