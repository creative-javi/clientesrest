package com.labs.creative.clientesrest.ui.editar;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.labs.creative.clientesrest.R;
import com.labs.creative.clientesrest.remote.api.ApiManager;
import com.labs.creative.clientesrest.remote.api.Constants;
import com.labs.creative.clientesrest.remote.api.IGetDataService;
import com.labs.creative.clientesrest.remote.model.BaseResponse;
import com.labs.creative.clientesrest.remote.model.ClienteResponse;
import com.labs.creative.clientesrest.ui.model.Cliente;
import com.labs.creative.clientesrest.utils.Network;
import com.labs.creative.clientesrest.utils.Tags;
import com.labs.creative.clientesrest.utils.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarActivity extends AppCompatActivity
{

    //Campos de edicion
    private TextInputEditText edIdCliente;
    private TextInputEditText edNombreCliente;
    private TextInputEditText edEdadCliente;
    private TextInputEditText edEmailCliente;
    private TextInputEditText edEstatusCliente;
    private Button btnEnviar;
    private Cliente cliente; //Objeto cliente que se va a editar
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);
        getSupportActionBar().setTitle(R.string.editar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    //Agrega navegacion a la actividad para quitar del Stack de activities el actual
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Inicializa las vistas
     */
    private void init()
    {
        context = this;
        edEdadCliente = findViewById(R.id.edEdad);
        edNombreCliente = findViewById(R.id.edNombre);
        edEmailCliente = findViewById(R.id.edEmail);
        edEstatusCliente = findViewById(R.id.edEstatus);
        edIdCliente = findViewById(R.id.edIdCliente);
        edIdCliente.setEnabled(false);
        btnEnviar = findViewById(R.id.btnEnviar);

        obtenerDatos(); //Obtiene los datos del intent
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Network.isNetworkAvailable(context)) //Verifica el estado de red
                {
                    enviarDatos(); //Envia los datos
                }
                else
                {
                    Util.showToast(context,context.getResources().getString(R.string.no_network));
                }
            }
        }); //Agrega el listener al boton
    }

    /**
     * Envia los datos al servidor
     */
    private void enviarDatos()
    {
        if (Util.validar(edEdadCliente.getText().toString()) || Util.validar(edNombreCliente.getText().toString()) ||
                Util.validar(edEstatusCliente.getText().toString())) //Valida los campos de texto
        {
            Util.showToast(context,getResources().getString(R.string.campos_incompletos));
        }
        else
        {
            /**Inicializamos el cliente
             * Asignamos sus propiedades con los setter y las obtenemos de los editText
             */
            cliente = new Cliente();
            cliente.setEmail(edEmailCliente.getText().toString());
            cliente.setNombre(edNombreCliente.getText().toString());
            cliente.setIdCliente(Integer.parseInt(edIdCliente.getText().toString()));
            cliente.setEdad(Integer.parseInt(edEdadCliente.getText().toString()));

            if (Integer.parseInt(edEstatusCliente.getText().toString()) > 1) //Valida el campo de estatus para que solo sea 1 para true
                                                                            //y 0 para false
                edEstatusCliente.setText("1");
            else if (Integer.parseInt(edEstatusCliente.getText().toString()) < 0)
                edEstatusCliente.setText("0");
            cliente.setEstatus(Integer.parseInt(edEstatusCliente.getText().toString()));
            IGetDataService service = ApiManager.createService(IGetDataService.class); //Crea el handler del servicio
            Call<BaseResponse> call = service.updateCliente(cliente); //Hace la peticion
            call.enqueue(new Callback<BaseResponse>()
            {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response)
                {
                    if (response.body() != null) //valida que la respuesta no sea nula
                    {
                        BaseResponse res = response.body(); //Asigna el body a la respuesta
                        if (res.getStatus() != Constants.OK_NUMBER) //Valida errores en el servidor
                            Util.showToast(context,"Ocurrio un error");  //Muestra un mensaje de error
                        else
                            Util.showToast(context,res.getMessage()); //Muestra el mensaje de exito
                    }
                    else
                    {
                        Util.showToast(context,"Ocurrio un error"); //Muestra mensaje de error
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) //Se ejecuta si falla la peticion
                {
                    Util.showToast(context,"Ocurrio un error");
                    Log.wtf("FAIL",t.getMessage());
                }
            });
        }

    }

    private void obtenerDatos()
    {
        edNombreCliente.setText(getIntent().getStringExtra(Tags.NOMBRE));
        edEmailCliente.setText(getIntent().getStringExtra(Tags.EMAIL));
        edIdCliente.setText(String.valueOf(getIntent().getIntExtra(Tags.ID,0)));
        edEdadCliente.setText(String.valueOf(getIntent().getIntExtra(Tags.EDAD,0)));
        edEstatusCliente.setText(String.valueOf(getIntent().getIntExtra(Tags.ESTATUS,0)));
    }
}
