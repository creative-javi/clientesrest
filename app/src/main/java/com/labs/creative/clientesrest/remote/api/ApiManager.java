package com.labs.creative.clientesrest.remote.api;
import com.labs.creative.clientesrest.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager
{
    //Crea el cliente http para registrar los logs
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(ApiConfiguration.TIME_OUT, ApiConfiguration.TIME_UNIT)
            .readTimeout(ApiConfiguration.TIME_OUT, ApiConfiguration.TIME_UNIT)
            .writeTimeout(ApiConfiguration.TIME_OUT, ApiConfiguration.TIME_UNIT)
            .addInterceptor(getLogs());

    //Crea el handler para las peticiones con una URL base.
    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(ApiConfiguration.BASE_URL_PROD).addConverterFactory(GsonConverterFactory.create());

    //Intercepta las bitacoras de las peticiones http para las pruebas. o en modo Debug
    private static HttpLoggingInterceptor getLogs() {
        if (BuildConfig.DEBUG) {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }
    }

    public static <S> S createService(Class<S> serviceClass) {
        return builder.client(httpClient.build()).build().create(serviceClass);
    }
}
