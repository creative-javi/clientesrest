package com.labs.creative.clientesrest.remote.model;

/**
 * Clase para una respuesta base
 * */
public class BaseResponse
{
    //propiedades en comun con sus Setter y Getter
    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
