package com.labs.creative.clientesrest.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.labs.creative.clientesrest.R;
import com.labs.creative.clientesrest.ui.agregar.AgregarActivity;
import com.labs.creative.clientesrest.ui.listado.ClientesActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    private CardView cvClientes;
    private CardView cvAgregar;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    /**
     * Inicializa las vistas y agrega los listeners para interactuar
     * */
    private void init()
    {
        cvClientes = findViewById(R.id.cvClientes);
        cvAgregar = findViewById(R.id.cvAgregar);
        cvClientes.setOnClickListener(this);
        cvAgregar.setOnClickListener(this);
    }

    //Se sobreescribe el metodo onClick para manejar los eventos
    @Override
    public void onClick(View v)
    {
        Intent siguiente; //Declara el intent para cambiar de actividad segun sea el caso

        //Obtenermos la vista que fue pulsada por medio de su id
        switch (v.getId())
        {
            case R.id.cvAgregar:
                siguiente = new Intent(this,AgregarActivity.class);
                startActivity(siguiente);
                break;
            case R.id.cvClientes:
                siguiente = new Intent(this, ClientesActivity.class);
                startActivity(siguiente);
                break;
        }
    }
}
