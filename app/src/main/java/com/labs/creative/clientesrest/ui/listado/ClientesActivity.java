package com.labs.creative.clientesrest.ui.listado;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.labs.creative.clientesrest.R;
import com.labs.creative.clientesrest.remote.api.ApiManager;
import com.labs.creative.clientesrest.remote.api.Constants;
import com.labs.creative.clientesrest.remote.api.IGetDataService;
import com.labs.creative.clientesrest.remote.model.BaseResponse;
import com.labs.creative.clientesrest.remote.model.ClienteResponse;
import com.labs.creative.clientesrest.ui.editar.EditarActivity;
import com.labs.creative.clientesrest.utils.Network;
import com.labs.creative.clientesrest.utils.Tags;
import com.labs.creative.clientesrest.utils.Util;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientesActivity extends AppCompatActivity
{
    private List<ClienteResponse> dataSet; //Dataset para guardar la respuesta del servidor
    private Context context; //Contexto en que se ejecuta la app
    private RecyclerView rvClientes; //Recycler View para mostrar la lista
    private RecyclerView.LayoutManager manager; //Layout para la alineacionnde los elementos del recycler
    private AdapterClientes adapter; //Adapatador para los elementos del recycler


    //Se ejecuta cuando se crea la vista
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);
        getSupportActionBar().setTitle(R.string.clientes);  //Cambia el titulo del actionbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //Activamos el boton de back
        init(); //Inicializamos los componentes de la UI
    }

    /**
     * Inicializa las vistas*
     */
    private void init()
    {
        context = this;
        manager = new LinearLayoutManager(this); //Se elige un LinearLayout puede ser tambien en forma de grid o staggered
        rvClientes = findViewById(R.id.rvClientes);
        rvClientes.setHasFixedSize(true); //Definimos un tamaño siempre estatico
        rvClientes.setLayoutManager(manager); //Añadimos el layout manager
        rvClientes.setDrawingCacheEnabled(true); //Habilitamos el cache
        rvClientes.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO); //Cache automatico


       if (Network.isNetworkAvailable(context)) //Verifica el estado de red
       {
           obtenerClientes(); //Obtiene los datos del servidor
       }
       else
       {
           Util.showToast(context,getResources().getString(R.string.no_network));
       }
    }

    //Agrega navegacion a la actividad para quitar del Stack de activities el actual
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Hace la peticion para obtener los clienes
     * */
    private void obtenerClientes()
    {
        IGetDataService service = ApiManager.createService(IGetDataService.class); //Crea el handler para utilizar los metodos del servicio
        Call<List<ClienteResponse>> call = service.getAllClientes(); //Hace la peticion
        call.enqueue(new Callback<List<ClienteResponse>>() {
            @Override
            public void onResponse(Call<List<ClienteResponse>> call, Response<List<ClienteResponse>> response) { // Se ejecuta si se hizo correctamente la solicitud
                procesarRespuesta(response.body()); //Invoca el motodo para procesar la respuesta
            }

            @Override
            public void onFailure(Call<List<ClienteResponse>> call, Throwable t) { //Se ejecuta si algo fallo en la peticion
                Log.wtf("FAIL",t.getMessage());
                Util.showToast(context,"Ocurrio un error");
            }
        });
    }

    /**Procesa las respuesta de la lista de los clientes
     *
     * @param response respuesta con la lista de datos del servidor
     * */
    private void procesarRespuesta(List<ClienteResponse> response)
    {
        if (response != null) //Verifica que la respuesta no este vacia
        {
            dataSet = response; //Asignamos la respuesta al conjunto de datos
            adapter = new AdapterClientes(dataSet,context); //Se crea el adaptador
            adapter.setListener(new OnItemClickListenerClientes()
            { //Asigna el listener al adapter para manejar los eventos
                @Override
                public void onDelete(final ClienteResponse item)
                {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context); // se crea una alerta de confirmación
                    alertDialog.setTitle(context.getResources().getString(R.string.eliminar));
                    alertDialog.setMessage(context.getResources().getString(R.string.confirmar));
                    //Agregamos un boton para confirmar
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which) {
                            eliminar(item); //Invoca el metodo eliminar
                        }
                    });
                    //Agregamos un boton de cancelar
                    alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Lo dejamos vacio para que simplemente se quite el dialogo
                        }
                    });
                    alertDialog.show(); //Muestra el dialogo
                }

                @Override
                public void onEdit(ClienteResponse item)
                {
                    editar(item); //Invoca el metodo para editar
                }
            });
            rvClientes.setAdapter(adapter); //Asigna el adaptador con los datos
        }
        else
        {
            Util.showToast(context,"Ocurrio un error");
        }
    }

    /**Hace la peticion para eliminar al cliente
     *
     * @param item  cliente a eliminar
     */
    private void eliminar(final ClienteResponse item)
    {
        IGetDataService service = ApiManager.createService(IGetDataService.class); //Crea el handler del servicio
        Call<BaseResponse> call = service.deleteCliente(item.getIdCliente()); //Crea la peticion para eliminar
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) //ejecuta si la peticion se hizo bien
            {
                if (response.body() != null) //Verifica que la respuesta no este vacia
                {
                    BaseResponse res = response.body();
                    if (res.getStatus() != Constants.OK_NUMBER) //Verifica que sea el codigo 200
                    {
                        Util.showToast(context,"Ocurrio un error");
                    }
                    else
                    {
                        dataSet.remove(item); //Se elimina de la lista
                        adapter.notifyDataSetChanged(); //Se notifica que el conjunto de datos cambio y refresca la vista
                        Util.showToast(context,res.getMessage()); //Se muestra el mensaje
                    }
                }
                else
                {
                    Util.showToast(context,"Ocurrio un error");
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) //Se ejecuta si falla la peticion
            {
                Util.showToast(context,"Ocurrio un error");
                Log.wtf("FAIL",t.getMessage());
            }
        });
    }

    /**Envia los datos del cliente al siguiente activity para editarlos
     *
     * @param item contiene toda la informacion del cliente a eliminar
     */
    private void editar(ClienteResponse item)
    {
        Intent siguiente = new Intent(this,EditarActivity.class);
        siguiente.putExtra(Tags.ID, item.getIdCliente());
        siguiente.putExtra(Tags.NOMBRE, item.getNombre());
        siguiente.putExtra(Tags.EDAD, item.getEdad());
        siguiente.putExtra(Tags.EMAIL, item.getEmail());
        siguiente.putExtra(Tags.ESTATUS, item.getEstatus());
        startActivity(siguiente);
    }
}
