package com.labs.creative.clientesrest.remote.api;

import java.util.concurrent.TimeUnit;

public class ApiConfiguration
{
    //Time out para las respuestas.
    public static final int TIME_OUT = 60;
    public static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    //Base URL
    //public static final String BASE_URL_PROD = "http://192.168.1.136:3000";
    public static final String BASE_URL_PROD = "http://192.168.42.175:3000";



}
